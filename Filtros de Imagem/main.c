﻿#include <stdio.h>
#include <stdlib.h>
#include "imagem.h" //Desenvolvida pelo prof. Bogdan.
/*============================================================================*/

char* ARQUIVOS [] = /*imagens da pasta img foram geradas pelo prof. Bogdan.*/
{
	   	"imgCelula01.bmp",
	   	"imgCelula02.bmp",
	    "imgTulipNoise.bmp",
	    "imgBorboleta.bmp",
	    "imgLenna.bmp",
    	"imgComponents.bmp",
    	"imgRice01.bmp",
		"imgRice02.bmp"

};

#define N_ARQUIVOS 8 /*est� testando apenas as 3 primeiras imagens da lista*/
#define TAM_JANELA 5

/******************************************************************
Prot�tipos
*******************************************************************/
void negativo(Imagem* img, Imagem* out);
void binariza(Imagem* img, Imagem* out, int limiar);
void filtroMedia(Imagem* img, Imagem* out, int winSize);
void filtroSobel(Imagem* img, Imagem* out);

void componentesConexos(Imagem* img);
int fowardScan(Imagem* img);
int backwardScan(Imagem* img);
int contaComponente(Imagem *img);

/******************************************************************
Fun��o principal
*******************************************************************/
int main ()
{
    int i, op, limiar, winSize;
    Imagem *img, *out; /* Imagens de entrada e sa�da*/
    char nome_saida [25]; /*String usada para armazenar os nomes das sa�das*/

    printf("\nO que deseja fazer?\n");
    printf("1 - negativo\n");
    printf("2 - binarizacao\n");
    printf("3 - filtro da media\n");
    printf("4 - componentes conexos \n");
    printf("5 - contagem de objetos\n");
    scanf("%d", &op);

    for (i = 0; i < N_ARQUIVOS; i++)
    {
        img = abreImagem (ARQUIVOS [i]);
        if (!img)
        {
            printf ("Nao conseguiu abrir %s\n", ARQUIVOS [i]);
            return (1);
        }
        printf ("Processando imagem %s...\n", ARQUIVOS [i]);

        out = criaImagem(img->largura, img->altura, 1);

        switch(op)
        {
        case 1:
            negativo(img, out);
            sprintf (nome_saida, "%s_negativ.bmp", ARQUIVOS [i]);
            break;
        case 2:
            printf("Qual o limiar desejado?\n");
            scanf("%d", &limiar);
            binariza(img, out, limiar);
            sprintf(nome_saida, "%s_bin.bmp", ARQUIVOS [i]);
            break;
        case 3:
            printf("Qual o winSize?\n");
            scanf("%d", &winSize);
            filtroMedia(img, out, winSize);
            sprintf(nome_saida, "%s_bin.bmp", ARQUIVOS [i]);
            break;
        case 4:
            componentesConexos(img);
            break;
        case 5:
            contaComponente(img);
            break;
        default:
            printf("Opcao invalida!");
        }
        salvaImagem (out, nome_saida);
        destroiImagem (out);
        destroiImagem (img);
    }


    return (0);
}
/******************************************************************
Fun��es
*******************************************************************/
void negativo(Imagem* img, Imagem* out)
{
    int i, j;

    for (i = 0; i < img->altura; i++)
        for (j = 0; j < img->largura; j++)
            out->dados[0][i][j] = 255 - img->dados[0][i][j];
}

void binariza(Imagem* img, Imagem* out, int limiar)
{
    int i, j;

    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
        {
            if(img->dados[0][i][j]>limiar)
                out->dados[0][i][j] = 255;
            else
                out->dados[0][i][j] = 0;
        }
    }
}

void filtroMedia(Imagem* img, Imagem* out, int winSize)
{
    int i, j, i2, j2, soma;

    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
        {
            soma=0;
            if(i<(winSize+1)/2 || j<(winSize+1)/2 || i+(winSize+1)/2>img->altura || j+(winSize+1)/2>img->largura)
                out->dados[0][i][j] = img->dados[0][i][j];
            else
            {
                for(i2=i-(winSize-1)/2; i2<i+(winSize-1)/2; i2++)
                    for(j2=j-(winSize-1)/2; j2<j+(winSize-1)/2; j2++)
                        soma+=img->dados[0][i2][j2];

                out->dados[0][i][j] = soma/(winSize*winSize);
            }
        }
    }
}

/****/
void componentesConexos(Imagem *img)
{
    int i, j;

    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
        {
            if(img->dados[0][i][j]==255)
                img->dados[0][i][j] = 1;
        }
    }

    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
            printf("%2d", img->dados[0][i][j]);
        putchar('\n');
    }
    putchar('\n');
    putchar('\n');

    for(i=1; fowardScan(img)!=0 && backwardScan(img)!=0; i++);

    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
            printf("%2d", img->dados[0][i][j]);
        putchar('\n');
    }
    putchar('\n');
    putchar('\n');


}

int fowardScan(Imagem *img)
{
    int i, j, mask = 1, min=0, ver = 0;

    for(i=1; i+1<img->altura; i++)
    {
        for(j=1; j+1<img->largura; j++)
        {
            if(img->dados[0][i][j]!=0)
            {
                if(img->dados[0][i][j-1]==0 && img->dados[0][i-1][j-1]==0 && img->dados[0][i-1][j]==0 && img->dados[0][i-1][j+1]==0)
                {
                    img->dados[0][i][j] = mask;
                    mask++;
                }
                else
                {
                    min = img->dados[0][i][j-1];

                    if(img->dados[0][i-1][j-1]!=0)
                    {
                        if(min == 0 || img->dados[0][i-1][j-1]<min)
                        {
                            min = img->dados[0][i-1][j-1];
                            ver = 1;
                        }
                    }
                    if(img->dados[0][i-1][j]!=0)
                    {
                        if(min == 0 || img->dados[0][i-1][j]<min)
                        {
                            min = img->dados[0][i-1][j];
                            ver = 1;
                        }
                    }
                    if(img->dados[0][i-1][j+1]!=0)
                    {
                        if(min == 0 || img->dados[0][i-1][j+1]<min)
                        {
                            min = img->dados[0][i-1][j+1];
                            ver = 1;
                        }
                    }
                    img->dados[0][i][j] = min;
                }
            }
        }
    }
    return ver;
}

int backwardScan(Imagem *img)
{
    int i, j, min = 0, ver = 0;

    for(i=(img->altura)-2; i>0; i--)
    {
        for(j=(img->largura)-2; j>0; j--)
        {
            if(img->dados[0][i][j]!=0)
            {
                min = img->dados[0][i][j+1];

                if(img->dados[0][i+1][j+1]!=0)
                {
                    if(min == 0 || img->dados[0][i+1][j+1]<min)
                    {
                        min = img->dados[0][i+1][j+1];
                        ver = 1;
                    }
                }
                if(img->dados[0][i+1][j]!=0)
                {
                    if(min == 0 || img->dados[0][i+1][j]<min)
                    {
                        min = img->dados[0][i+1][j];
                        ver = 1;
                    }
                }
                if(img->dados[0][i+1][j-1]!=0)
                {
                    if(min == 0 || img->dados[0][i+1][j-1]<min)
                    {
                        min = img->dados[0][i+1][j-1];
                        ver = 1;
                    }
                }
                if(min!=0)
                    img->dados[0][i][j] = min;
            }
        }
    }
    return ver;
}
int contaComponente(Imagem *img)
{
    int i, j, max = img->dados[0][0][0];
    for(i=0; i<img->altura; i++)
    {
        for(j=0; j<img->largura; j++)
        {
            if(img->dados[0][i][j]>max)
                max=img->dados[0][i][j];
        }
    }
    return max;
}
/* componentes conexos???
 contagem de objetos???*/
